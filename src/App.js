import {AiFillLinkedin, AiFillGitlab} from 'react-icons/ai'
import {MdPublic} from 'react-icons/md'

export default function App() {
  return (
    <div>
      <section className='bg-gray-100 border-b border-b-gray-700 md:pb-4'>
        <nav className='py-5 px-10 flex justify-between'>
          <h1 className='text-2xl mr-4 md:text-5xl'><span className='text-purple-800'>Brady</span> <b>Ketelsen</b></h1>
          <ul className='flex items-center'>
            <li><a href='http://www.linkedin.com/in/brady-ketelsen'><AiFillLinkedin className='cursor-pointer text-3xl mr-2 fill-purple-600'/></a></li>
            <li><a href='http://gitlab.com/Brady.ketelsen'><AiFillGitlab className='cursor-pointer text-3xl mr-3 fill-purple-600'/></a></li>
            <li><a className='px-3 py-1 rounded-md bg-purple-600 cursor-pointer shadow-lg text-xl text-white' href='https://drive.google.com/file/d/1-tsBSkTJhf3SjwCkDbttCH6ysKJlV9D9/view?usp=sharing'>Resume</a></li>
          </ul>
        </nav>
      </section>
      <section className='bg-gray-300 pt-2 pb-5 md:pb-20'>
        <div className='text-center'>
          <h3 className='mb-12 md:mb-20 text-sm md:text-md'>A curious, creative engineer who possesses a passion for powerful, ergonomic applications with intuitive, accessible design</h3>
        </div>
        <div className='mb-10'>
        <div className='rounded-full h-80 w-80 overflow-hidden mx-auto m-10'>
          <img src={process.env.PUBLIC_URL + '/round-headshot.jpg'} alt='heahot' className='mb-8'/>
        </div>
        <div>
          <h1 className='text-5xl pl-10 md:pl-[25%]'>About Me</h1>
          <br></br>
          <p className='px-10 md:px-[20%] leading-8 md:leading-10 text-gray-800 text-lg md:text-2xl'>I am a motivated and resourceful aspiring software engineer, expanding my skills building smaller projects both solo and collaboratively. My philosophy is to find the simplest solution for difficult problems and to make technology that is accessible and intuitive. I’m looking to join a team of innovative professionals with similar mindsets for growth and human-centric app development.
          </p>
          <br></br>
          <p className='px-10 md:px-[20%] leading-8 md:leading-10 text-gray-800 text-lg md:text-2xl'>As a former music educator, I put on creative and moving shows. These productions required the management of many performers, collaboration with other artists, soothing of logistical difficulties, and the most tireless can-do spirit. While working on smaller projects to hone my software skills, I am still an active musician singing with the Philadelphia Gay Men’s Chorus and playing piano for church services.</p>
        </div>
        </div>
      </section>
      <section className='bg-gray-600 pb-10'>
        <div>
          <h1 className='text-5xl pl-10 pt-7 md:pt-32 md:pl-[25%] mb-10 md:mb-24 text-white'>Tools</h1>

        </div>
        <div className='flex gap-4 pl-10 md:px-[20%] md:pb-20 flex-wrap'>
          <div className='border border-black rounded-xl h-30 w-30 bg-gray-100'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/react.png'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>React</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/python.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Python</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/javascript.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Javascript</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/html5.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>HTML</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/css3.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>CSS</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/tailwind.png'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Tailwind CSS</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-7 pt-2 h-[130px] w-[130px]' src={process.env.PUBLIC_URL + '/icons/fastapi.svg'} alt='react icon'/>
            <p className='text-center mb-3 text-lg'>FastAPI</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/django.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Django</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/sql.png'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>SQL</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/docker.png'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Docker</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/git.png'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>Git</p>
          </div>
          <div className='bg-gray-100 border border-black rounded-xl h-30 w-30'>
            <img className='px-4 pt-4' src={process.env.PUBLIC_URL + '/icons/postgresql.svg'} alt='react icon'/>
            <p className='text-center mt-2 mb-3 text-lg'>PostgreSQL</p>
          </div>

        </div>
      </section>
<section className='py-10 md:py-32 bg-slate-800'>
  <div>
    <h1 className='text-white text-5xl pl-10 md:pl-[25%] mb-10 md:mb-20'>Projects</h1>
  </div>
  <div className='flex px-6 md:px-[15%] flex-wrap gap-10'>
    <div className='w-[350px] h-[450px] bg-white rounded-md'>
      <div className='flex ml-4 my-6'>
      <h1 className='text-2xl'><b>tasteful</b></h1>
      <div className='flex ml-auto mr-2'>
        <a className='pr-2' href='https://tastefulapp.gitlab.io/tasteful/'>
         <MdPublic className='text-3xl cursor-pointer  fill-purple-600'/>
        </a>
        <a href='https://gitlab.com/tastefulapp/tasteful'>
          <AiFillGitlab className='text-3xl cursor-pointer fill-purple-600'/>
        </a>
      </div>
      </div>
      <img className='w-[90%] m-4 border border-black' src={process.env.PUBLIC_URL + '/screenshots/tasteful-map.png'} alt='tasteful map'/>
      <p className='mx-4 mb-6'>Discover and review dishes at restaurants near you!</p>
      <p className='mx-4 mb-2'>FastAPI | Python | SQL | React | Javascript | HTML | CSS</p>
    </div>
    <div className='w-[350px] h-[450px] bg-white rounded-md'>
      <div className='flex ml-4 my-6'>
      <h1 className='text-2xl'><b>CarCar</b></h1>
        <a className='ml-auto mr-2' href='https://gitlab.com/LithiumAndGold/carcar'>
          <AiFillGitlab className='text-3xl cursor-pointer fill-purple-600'/>
        </a>
      </div>
      <img className='w-[90%] m-4 border border-black' src={process.env.PUBLIC_URL + '/screenshots/carcar-appt.png'} alt='carcar form'/>
      <p className='mx-4 mb-6'>An app for a car dealership to manage their inventory and employees.</p>
      <p className='mx-4 mb-2'>Django | Python | React | Javascript | HTML | CSS</p>
    </div>
    <div className='w-[350px] h-[450px] bg-white rounded-md'>
      <div className='flex ml-4 my-6'>
      <h1 className='text-2xl'><b>Pantry Picker</b></h1>
        <a className='ml-auto mr-2' href='https://gitlab.com/Brady.ketelsen/pantry-picker'>
          <AiFillGitlab className='text-3xl cursor-pointer fill-purple-600'/>
        </a>
      </div>
      <img className='w-[90%] m-4 border border-black' src={process.env.PUBLIC_URL + '/screenshots/pantry-picker-pantry.png'} alt='pantry'/>
      <p className='mx-4 mb-6'>Track your pantry and find recipes to make with what you have.</p>
      <p className='mx-4 mb-2'>Django | Python | HTML | CSS</p>
    </div>
    <div className='w-[350px] h-[450px] bg-white rounded-md'>
      <div className='flex ml-4 my-6'>
      <h1 className='text-2xl'><b>Voicify</b></h1>
        <a className='ml-auto mr-2' href='https://gitlab.com/Brady.ketelsen/voice-lesson-app'>
          <AiFillGitlab className='text-3xl cursor-pointer fill-purple-600'/>
        </a>
      </div>
      <img className='w-[90%] m-4 border border-black' src={process.env.PUBLIC_URL + '/screenshots/voicify-welcome.png'} alt='voicify welcome'/>
      <p className='mx-4 mb-6'>Keep track of voice lessons given and payments received. </p>
      <p className='mx-4 mb-2'>Django | Python | HTML | CSS</p>
    </div>
  </div>
</section>
<section className='py-10 px-10 bg-gray-600 border-t border-t-black'>
  <div className='text-center'>
    <h1 className='md:text-xl text-white'>Feel free to contact me using the information in my resume located at the top of this page. Thank you!</h1>
  </div>
</section>


    </div>
  );
}
